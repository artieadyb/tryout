const isMatch = (expr)=>{
   //buat variabel penampung
   const content= []
   //cek semua isi expr
   for (let i of expr){
      // console.log(i)
      if (i == '(' ||i == '[' || i == '{') {
         content.push(i);
         continue; 
      }

      if (content.length === 0) {
          return false
      }
      //cari pasangan nya dengan case tertentu untuk menghapus element terakhir expr
      switch (i) {
         case ')':
              content.pop();
              if (i == '{' || i == '[') {
                return false  
              }
              break;
         case '}':
              content.pop();
              if (i == '(' || i == '[') {
                return false  
              }
              break;
         case ']':
              content.pop();
              if (i == '{' || i == '(') {
                return false  
              }
              break;
     }
   }
   //jika content masih memiliki element maka return false.
  return content.length ? false : true

   // console.log(content)
}

console.log(isMatch('}{)(]['))

